const {Load, loadJoiSchema} = require('./models/Loads.js');
const {User} = require('./models/Users.js');
const {Truck} = require('./models/Trucks.js');

async function createLoad(req, res, next) {
  try {
    const {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions: {width, length, height},
    } = req.body;

    await loadJoiSchema.validateAsync({
      name,
      payload,
      pickup_address,
      delivery_address,
      width,
      length,
      height,
    });

    const load = new Load({
      created_by: req.user.userId,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions: {width, length, height},
    });

    load.save().then((saved) => {
      res.json({
        message: 'Load created successfully',
        load: saved,
      });
    });
  } catch (err) {
    if (err) throw err;
  }
}

async function getLoads(req, res, next) {
  const {status, offset, limit} = req.query;

  const searchParams = {
    $or: [{created_by: req.user.userId}, {assigned_to: req.user.userId}],
  };
  if (limit) {
    searchParams.limit = limit;
  }
  if (offset) {
    searchParams.offset = offset;
  }

  try {
    await User.findById(req.user.userId).then(async (response) => {
      if (response.role === 'DRIVER') {
        if (status && status !== 'NEW') {
          searchParams.status = status;
        }
        await Load.find(searchParams)
            .skip(offset || 0)
            .limit(limit || 5)
            .then((result) => {
              if (result.length === 0) throw new Error('no loads');

              return res.status(200).send({message: `Success`, loads: result});
            });
      }

      if (response.role === 'SHIPPER') {
        if (status) {
          searchParams.status = status;
        }
        const loadTotal = await Load.find(searchParams);

        await Load.find(searchParams)
            .skip(offset || 0)
            .limit(limit || 5)
            .then((result) => {
              if (result.length === 0) throw new Error('no loads');

              return res.status(200).send({
                message: `Success`,
                loads: result,
                total: loadTotal.length,
              });
            });
      }
    });
  } catch (e) {
    if (e.message === 'no loads') {
      return res.status(400).send({message: `No loads to show`});
    }
  }
}

async function getByIdLoad(req, res, next) {
  try {
    await Load.find({
      _id: req.params.id,
      $or: [{assigned_to: req.user.userId}, {created_by: req.user.userId}],
    }).then((result) => {
      if (result.length === 0) {
        return res
            .status(400)
            .send({message: `No load with this id to show`});
      }
      res.json({
        load: result,
      });
    });
  } catch (err) {
    if (err) throw err;
  }
}

async function postLoad(req, res, next) {
  const load = await Load.findById({_id: req.params.id});

  const updStatus = Load.schema.path('status').enumValues;
  load.status = updStatus[1];

  if (load.assigned_to) {
    return res.status(400).json({message: 'Load is already assigned'});
  }

  const truck = await Truck.findOne({
    assigned_to: {$ne: null},
    status: 'IS',
  });

  try {
    if (!truck) {
      load.status = updStatus[0];
      return res.status(400).json({message: 'No driver found'});
    } else {
      if (load.payload > truck.payload) {
        load.status = updStatus[0];
        return res.status(400).json({message: 'Load is to heavy'});
      }

      if (
        load.dimensions.width > truck.dimensions.width ||
        load.dimensions.length > truck.dimensions.length ||
        load.dimensions.height > truck.dimensions.height
      ) {
        load.status = updStatus[0];
        return res.status(400).json({message: 'Load is to heavy'});
      }

      load.assigned_to = truck.assigned_to;
      truck.status = 'OL';

      load.status = updStatus[2];

      const updState = Load.schema.path('state').enumValues;
      load.state = updState[0];

      load.logs.message = `Load successfully posted and assigned to driver ${truck.assigned_to} with truck ${truck._id}`;
      load.logs.time = new Date();

      await truck.save();
      await load.save();

      res.json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }
  } catch (err) {}
}

const getActiveLoad = async (req, res, next) => {
  try {
    await Load.find({
      assigned_to: req.user.userId,
      $or: [{status: 'NEW'}, {status: 'POSTED'}, {status: 'ASSIGNED'}],
    }).then((result) => {
      if (result.length === 0) {
        return res.status(400).send({message: `No active loads`});
      }
      return res.status(200).send({load: result[0]});
    });
  } catch (err) {
    if (err) throw err;
  }
};

const changeActiveStateLoad = async (req, res, next) => {
  try {
    const load = await Load.findOne({
      assigned_to: req.user.userId,
      status: {$ne: 'SHIPPED'},
    });

    if (!load) throw new Error('no load');

    const loadStates = Load.schema.path('state').enumValues;
    const currentStateIndex = loadStates.indexOf(load.state);

    if (currentStateIndex === loadStates.length - 1 ) {
      return res.json({
        message: `Nothing to change`,
        load: load,
      });
    }

    if (currentStateIndex === 2) {
      load.status = 'SHIPPED';
    }

    load.state = loadStates[currentStateIndex + 1];
    load.logs.message = `Load state changed to ${load.state}`;

    load.logs.time = new Date();

    return load.save().then((saved) => {
      res.json({
        message: `Load state changed to '${load.state}'`,
        load: saved,
      });
    });
  } catch (err) {
    if (err.message === 'no load') {
      return res.status(400).send({message: `No loads assigned`});
    }
  }
};

const updateLoad = async (req, res, next) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions: {width, length, height},
  } = req.body;

  try {
    const load = await Load.findOne({_id: req.params.id, status: 'NEW'});

    if (!load) {
      return res.status(400).send({message: `No load to show`});
    }

    load.name = name;
    load.payload = payload;
    load.pickup_address = pickup_address;
    load.delivery_address = delivery_address;
    load.dimensions.width = width;
    load.dimensions.length = length;
    load.dimensions.height = height;

    load.logs.message = `Load data was updated`;
    load.logs.time = new Date();

    return load.save().then((saved) => {
      res.json({
        load: saved,
      });
    });
  } catch (err) {
    if (err) throw err;
  }
};

const deleteLoad = async (req, res, next) => {
  try {
    await Load.findOneAndRemove({
      userId: req.user.userId,
      _id: req.params.id,
      status: 'NEW',
    }).then((load) => {
      if (!load) {
        return res.status(400).send({message: `No loads with this id`});
      }

      res.status(200).send({message: `Load deleted successfully`});
    });
  } catch (err) {
    if (err) throw err;
  }
};

async function getShippingInfo(req, res, next) {
  try {
    const load = await Load.findById({
      _id: req.params.id,
      status: {$ne: 'SHIPPED'},
    });
    const truck = await Truck.findOne({
      assigned_to: load.assigned_to,
      status: 'OL',
    });

    res.json({load: load, truck: truck});
  } catch (err) {
    if (err) throw err;
  }
}

module.exports = {
  createLoad,
  getLoads,
  postLoad,
  getActiveLoad,
  changeActiveStateLoad,
  getByIdLoad,
  updateLoad,
  deleteLoad,
  getShippingInfo,
};
