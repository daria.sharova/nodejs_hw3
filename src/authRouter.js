const express = require('express');
const router = express.Router();
const {
  registerUser,
  loginUser,
  forgotPass,
  resetPass,
} = require('./authService.js');
const path = require('path');

router.post('/register', registerUser);

router.post('/login', loginUser);

router.post('/forgot_password', forgotPass);

router.post('/reset/:id/:token', resetPass);

router.get('/reset/:id/:token', function(req, res) {
  res.sendFile(path.resolve(__dirname + '/..' + '/public/reset.html'));
});

module.exports = {
  authRouter: router,
};
