const {Truck} = require('./models/Trucks.js');

function createTruck(req, res, next) {
  try {
    const {type} = req.body;

    if (!type) {
      return res.status(400).send({message: `Truck type is required`});
    }

    const truck = new Truck({
      created_by: req.user.userId,
      type,
    });

    if (type === 'SPRINTER') {
      truck.dimensions.width = 300;
      truck.dimensions.length = 250;
      truck.dimensions.height = 170;
      truck.payload = 1700;
    }

    if (type === 'SMALL STRAIGHT') {
      truck.dimensions.width = 500;
      truck.dimensions.length = 250;
      truck.dimensions.height = 170;
      truck.payload = 2500;
    }

    if (type === 'LARGE STRAIGHT') {
      truck.dimensions.width = 700;
      truck.dimensions.length = 350;
      truck.dimensions.height = 200;
      truck.payload = 4000;
    }

    truck.save().then((saved) => {
      res.json({
        message: 'Truck created successfully',
        truck: saved,
      });
    });
  } catch (err) {
    if (err) throw err;
  }
}

function getTrucks(req, res, next) {
  try {
    Truck.find({created_by: req.user.userId}).then((result) => {
      if (result.length === 0) {
        return res.status(400).send({message: `No trucks to show`});
      }

      res.json({
        trucks: result,
      });
    });
  } catch (err) {
    if (err) throw err;
  }
}

const getTruck = (req, res, next) => {
  try {
    Truck.find({userId: req.user.userId, _id: req.params.id}).then(
        (result) => {
          if (result.length === 0) {
            return res
                .status(400)
                .send({message: `No ${req.user.username} trucks with this id`});
          }
          res.json({note: result[0]});
        },
    );
  } catch (err) {
    if (err) throw err;
  }
};

const updateTruck = async (req, res, next) => {
  try {
    const truck = await Truck.find({
      userId: req.user.userId,
      _id: req.params.id,
    });

    const {type} = req.body;

    if (type) truck[0].type = type;

    return truck[0]
        .save()
        .then((saved) =>
          res.json({
            message: 'Truck details changed successfully',
            truck: saved,
          }),
        );
  } catch (err) {
    if (err) throw err;
  }
};

const deleteTruck = (req, res, next) => {
  try {
    Truck.findOneAndRemove({
      userId: req.user.userId,
      _id: req.params.id,
    }).then((truck) => {
      if (!truck) {
        return res
            .status(400)
            .send({message: `No ${req.user.username} trucks with this id`});
      }

      res.status(200).send({message: `Truck deleted successfully`});
    });
  } catch (err) {
    if (err) throw err;
  }
};

const assignTruck = async (req, res, next) => {
  try {
    const truck = await Truck.find({
      userId: req.user.userId,
      _id: req.params.id,
    });

    if (!truck[0].assigned_to) truck[0].assigned_to = req.user.userId;

    return truck[0]
        .save()
        .then((saved) =>
          res.json({message: 'Truck assigned successfully', truck: saved}),
        );
  } catch (err) {
    if (err) throw err;
  }
};

module.exports = {
  createTruck,
  getTrucks,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};
