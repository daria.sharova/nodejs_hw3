const {User} = require('../models/Users.js');

const authMiddlewareDriver = (req, res, next) => {
  try {
    User.findById(req.user.userId).then((response) => {
      console.log(response.role);
      if (response.role !== 'DRIVER') {
        console.log('test');
        return res.status(400).json({message: `Only drivers can access`});
      }
      next();
    });
  } catch (err) {
    if (err) throw err;
  }
};

module.exports = {
  authMiddlewareDriver,
};
