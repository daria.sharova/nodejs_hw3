const {User} = require('../models/Users.js');

const authMiddlewareShipper = (req, res, next) => {
  try {
    User.findById(req.user.userId).then((response) => {
      console.log(response.role);
      if (response.role !== 'SHIPPER') {
        console.log('test');
        return res.status(400).json({message: `Only shipper can do this action`});
      }
      next();
    });
  } catch (err) {
    if (err) throw err;
  }
};

module.exports = {
  authMiddlewareShipper,
};
