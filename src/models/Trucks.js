const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {

  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  dimensions: {
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },
  payload: {
    type: Number,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },

});

module.exports = {
  Truck,
};
