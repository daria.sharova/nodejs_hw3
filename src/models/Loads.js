const mongoose = require('mongoose');

const Joi = require('joi');

const loadJoiSchema = Joi.object({
  name: Joi.string().min(3).max(100).required(),
  payload: Joi.number().integer().min(1).max(2400).required(),
  pickup_address: Joi.string().min(3).max(300).required(),
  delivery_address: Joi.string().min(3).max(300).required(),
  width: Joi.number().integer().min(1).max(100).required(),
  length: Joi.number().integer().min(1).max(100).required(),
  height: Joi.number().integer().min(1).max(100).required(),
});

const Load = mongoose.model('Load', {
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ],
  },
  name: {
    type: String,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },
  logs: {
    message: {
      type: String,
      default: 'Load log',
    },
    time: {
      type: Date,
      default: Date.now,
    },
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = {
  Load,
  loadJoiSchema,
};
