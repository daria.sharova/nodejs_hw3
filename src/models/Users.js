const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: {allow: ['com', 'net']},
  }),

  password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
});

const User = mongoose.model('User', {
  image: {
    data: Buffer,
    type: String,
    default: 'no-image.png',
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  role: {
    type: String,
    required: true,
    enum: ['SHIPPER', 'DRIVER'],
  },
});

module.exports = {
  User,
  userJoiSchema,
};
