const {User, userJoiSchema} = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const salt = bcrypt.genSaltSync(10);
const sgMail = require('@sendgrid/mail');

const registerUser = async (req, res, next) => {
  const {email, password, role} = req.body;

  await userJoiSchema.validateAsync({email, password});

  const user = new User({
    email,
    password: await bcrypt.hash(password, salt),
    role,
  });

  user
      .save()
      .then((saved) =>
        res.json({
          message: 'Profile created successfully',
        }),
      )
      .catch((err) => {
        next(err);
      });
};

const loginUser = async (req, res, next) => {
  try {
    const {email, password} = req.body;

    await userJoiSchema.validateAsync({email, password});

    const user = await User.findOne({email: email});

    if (!user) {
      throw new Error('no user');
    }

    if (
      user &&
      (await bcrypt.compare(String(password), String(user.password)))
    ) {
      const payload = {email: user.email, userId: user._id};
      const jwt_token = jwt.sign(payload, 'secret-jwt-key');

      return res.json({
        message: `Welcome, ${user.role}`,
        jwt_token: jwt_token,
        role: user.role,
      });
    }
    throw new Error('no auth');
    // return res.status(400).json({ message: 'Not authorized' });
  } catch (e) {
    if (e.message === 'no user') {
      res.status(400).json({message: 'No user with this email'});
    }

    if (e.message === 'no auth') {
      res.status(400).json({message: 'Not authorized'});
    }
  }
};

const forgotPass = async (req, res, next) => {
  const {email} = req.body.email;

  await userJoiSchema.validateAsync({email});

  try {
    const user = await User.findOne({email: email});

    if (!user) throw new Error('no user');

    const token = () => {
      const payload = {userId: user._id};
      const secretKey = user.password + '-' + user.createdDate;
      const jwt_tokenTemporary = jwt.sign(payload, secretKey, {
        expiresIn: 3600, // 1 hour
      });
      return jwt_tokenTemporary;
    };

    const link = `http://localhost:8080/api/auth/reset/${user._id}/${token()}`;

    await sendEmail(user.email, 'Password reset', link);

    res.status(200).send('password reset link sent to your email account');

    async function sendEmail(email, subject, text) {
      try {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY);

        const msg = {
          to: email, // Change to your recipient
          from: 'shturvalova.m@gmail.com', // Change to your verified sender
          subject: subject,
          text: `Go to link ${text}`,
          html: `<strong>Go to ${text}</strong>`,
        };

        sgMail
            .send(msg)
            .then((response) => {
              console.log(response);
            })
            .catch((error) => {
              console.error(error);
            });
      } catch (error) {
        console.log(error, 'email not sent');
      }
    }
  } catch (err) {
    if (err.message === 'no user') {
      res.status(400).send({message: `user with given email doesn't exist`});
    }
  }
};

const resetPass = async (req, res, next) => {
  const {id} = req.params;
  const {password} = req.body;

  await userJoiSchema.validateAsync({password});

  try {
    const user = await User.findOneAndUpdate(
        {_id: id},
        {password: await bcrypt.hash(password, salt)},
    );

    if (!user) throw new Error('no user');

    res.status(200).send({message: `User updated password successfully`});
  } catch (err) {
    if (err.message === 'no user') {
      res.status(400).send({message: `No user found`});
    }
  }
};

module.exports = {
  registerUser,
  loginUser,
  forgotPass,
  resetPass,
};
