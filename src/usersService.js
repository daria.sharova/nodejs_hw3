const {User} = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const path = require('path');
const salt = bcrypt.genSaltSync(10);

const showUser = async (req, res, next) => {
  try {
    User.findById(req.user.userId).then((response) => {
      res.json({
        user: {
          _id: response._id,
          image: response.image,
          role: response.role,
          email: response.email,
          createdDate: response.createdDate,
        },
      });
    });
  } catch {
    if (err) throw err;
  }
};

const deleteUser = async (req, res, next) => {
  User.findByIdAndDelete(req.user.userId).then((user) => {
    res.status(200).send({message: `Profile deleted successfully`});
  });
};

const updateUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);
    const {oldPassword, newPassword} = req.body;

    if (bcrypt.compare(String(oldPassword), String(user.password))) {
      user.password = await bcrypt.hash(newPassword, salt);
      return user.save().then(() => {
        res.status(200).send({message: `Password changed successfully`});
      });
    } else {
      throw new Error('no match');
    }
  } catch (err) {
    if (err.message === 'no match') {
      res.status(400).send({message: `Old password doesn't match`});
    }
  }
};

const uploadUserImage = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);

    if (!req.files || Object.keys(req.files).length === 0) {
      throw new Error('no files');
    }

    const sampleFile = req.files.image;
    user.image = sampleFile.name;

    const uploadPath = path.resolve(__dirname, 'uploads', sampleFile.name);
    sampleFile.mv(uploadPath);

    await user.save();

    res.status(200).send({message: 'Avatar successfully changed'});
  } catch (e) {
    if (e.message === 'no files') {
      return res.status(400).send({message: 'No files were uploaded.'});
    }
  }
};

module.exports = {
  showUser,
  deleteUser,
  updateUser,
  uploadUserImage,
};
