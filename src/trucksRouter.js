const express = require('express');
const router = express.Router();
const {
  createTruck,
  getTrucks,
  getTruck,
  deleteTruck,
  updateTruck,
  assignTruck,
} = require('./truckService.js');

const {authMiddleware} = require('./middleware/authMiddleware');
const {authMiddlewareDriver} = require('./middleware/authMiddlewareDriver');

router.post('/', authMiddleware, authMiddlewareDriver, createTruck);

router.post('/:id/assign', authMiddleware, authMiddlewareDriver, assignTruck);

router.get('/', authMiddleware, authMiddlewareDriver, getTrucks);

router.get('/:id', authMiddleware, authMiddlewareDriver, getTruck);

router.put('/:id', authMiddleware, authMiddlewareDriver, updateTruck);

router.delete('/:id', authMiddleware, authMiddlewareDriver, deleteTruck);

module.exports = {
  truckRouter: router,
};
