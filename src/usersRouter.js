const express = require('express');
const router = express.Router();

const {showUser, deleteUser, updateUser, uploadUserImage} = require('./usersService.js');
const {authMiddleware} = require('./middleware/authMiddleware.js');

router.get('/', authMiddleware, showUser);

router.post('/', authMiddleware, uploadUserImage);

router.delete('/', authMiddleware, deleteUser);

router.patch('/password', authMiddleware, updateUser);

module.exports = {
  userRouter: router,
};
