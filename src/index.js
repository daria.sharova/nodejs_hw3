const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const fileUpload = require('express-fileupload');
const mongoose = require('mongoose');

const app = express();

require('dotenv').config();

mongoose.connect('mongodb+srv://TakeFive:gUx7eqNNVo3M7xju@cluster0.mmjwf0j.mongodb.net/?retryWrites=true&w=majority');

const {authRouter} = require('./authRouter.js');
const {userRouter} = require('./usersRouter.js');
const {truckRouter} = require('./trucksRouter.js');
const {loadRouter} = require('./loadRouter.js');

app.use(express.json());
app.use(morgan('tiny', {stream: fs.createWriteStream(
    path.resolve(__dirname, 'access.log'),
    {flags: 'a'},
)}));

morgan.token('custom', `
:http-version request (:method) for 
:url => successeded 
:status it took 
:total-time[digits] milliseconds`);

const skipLog = (req, res) =>
  res.statusCode >= 400 || req.method === 'GET' || req.method === 'DELETE';

app.use(morgan('custom', {
  skip: skipLog,
  stream: fs.createWriteStream(
      path.resolve(__dirname, 'operations.log'),
      {flags: 'a'},
  )}));

app.use(express.static('public'));
app.use('/uploads', express.static('src/uploads'));

app.use(fileUpload({}));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

const start = async () => {
  try {
    app.listen(process.env.PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error('err', err);
  res.status(500).send({'message': 'Server error'});
}
