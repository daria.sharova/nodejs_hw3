const express = require('express');
const router = express.Router();
const {
  createLoad,
  getLoads,
  postLoad,
  getActiveLoad,
  changeActiveStateLoad,
  getByIdLoad,
  updateLoad,
  deleteLoad,
  getShippingInfo,

} = require('./loadService.js');

const {authMiddleware} = require('./middleware/authMiddleware');
const {authMiddlewareDriver} = require('./middleware/authMiddlewareDriver');
const {authMiddlewareShipper} = require('./middleware/authMiddlewareShipper');

router.post('/', authMiddleware, authMiddlewareShipper, createLoad);

router.post('/:id/post', authMiddleware, authMiddlewareShipper, postLoad);

router.get('/', authMiddleware, getLoads);

router.get('/active', authMiddleware, authMiddlewareDriver, getActiveLoad);

router.get('/:id', authMiddleware, getByIdLoad);

router.get('/:id/shipping_info', authMiddleware, authMiddlewareShipper, getShippingInfo);

router.patch('/active/state', authMiddleware, authMiddlewareDriver, changeActiveStateLoad);

router.put('/:id', authMiddleware, authMiddlewareShipper, updateLoad);

router.delete('/:id', authMiddleware, authMiddlewareShipper, deleteLoad);

module.exports = {
  loadRouter: router,
};
