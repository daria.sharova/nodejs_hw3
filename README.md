# My Delivery App

Simple app homework to make shipping operations easier. Users of the app are shippers and drivers where they can simply communicate, see shipment info and interact with loads. Simple to use app is a great tool for transport or delivery services business.

## Get started

### for users

To start an app simply register, choosing your respective role and log in with your email and password.
Then the main page will appear with some basic interactive info about loads and trucks, depending on your role. Also profile page and other options are provided.

### for developers

Dillinger requires [Node.js](https://nodejs.org/) v10+ to run.

Open project in your editing software and install all necessary packages by putting install command in your terminal:
```sh
npm install
```
This should download and save all the dependencies and devDependencies to edit code easily.
Start the server by putting the following command to your terminal:
```sh
npm start
```
Enjoy coding.

Verify the deployment by navigating to your server address in
your preferred browser.

```sh
localhost:8080
```

## Basic features

This app is simple though has some all needed features to operate loads and fast delivery.

- Drivers can create trucks and assign them for future rides.
- Shippers create and post loads to be picked-up by available trucks to be shipped to their destination.
- Driver can interact with assigned to him load and change it's status
- Shipper has a possibility to view loads and shipment info

## Additional features

- Each user can reset his password using 'forgot password' option

## In-process features

- User can upload avatar
- User can download information in excel format

## Tech

App uses a number of open source projects to work properly:

- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework
- [Postman] - API platform for building and using APIs
- [Mongodb] - Fully Managed Database Service Automated Deployments & Config

And of course the App itself is open source with a [public repository][https://gitlab.com/daria.sharova/nodejs_hw3]
 on GitLab.

## License

Daria Sharova,
EPAM LAB

   [node.js]: <http://nodejs.org>
   [express]: <http://expressjs.com>
   [Mongodb]: <https://www.mongodb.com/>
   [Postman]: <https://www.postman.com/>