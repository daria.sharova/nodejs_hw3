import {changePassForm} from './auth.js';
import {app} from './app.js';
import {deleteAccount, uploadAvatar} from './userService.js';

const profile = (role, email, avatar) => {
  document.querySelector('.container').innerHTML = '';

  const profileContainer = document.createElement('div');
  profileContainer.classList.add('profile');
  const template = `
  <div class="avatar"><img src="${avatar}"  alt="avatar"></div>
  <h3 class="user-greeting">Welcome ${email}</h3>
  <div class="profile-info">Your role is ${role}</div>
  <div class="change-avatar">
    <form action="#" id="avatar-form" enctype="multipart/form-data">
      <div>
        <label for="image">Upload Image</label>
        <input type="file" id="image" name="image" accept="image/*" required />
      </div>
      <div>
        <button type="submit">Upload</button>
      </div>
    </form>
  </div> 
  <a href="#" class="change-avatar-link">Change avatar</a>
  <a href="#" class="change-pass-link">Change pass</a>
  <a href="#" class="delete-account">Delete account</a>
  <a href="#" class="back-to-app">Go back</a>
  `;
  profileContainer.innerHTML = template;
  document.querySelector('.container').append(profileContainer);
  document.querySelector('.change-pass-link').addEventListener('click', changePassForm);
  document.querySelector('.change-avatar-link').addEventListener('click', (e) => {
    document.querySelector('.change-avatar').style.display = 'block';
  });
  document.querySelector('.delete-account').addEventListener('click', deleteAccount);
  document.querySelector('.back-to-app').addEventListener('click', (e) => {
    app();
  });
  document.getElementById('avatar-form').addEventListener('submit', uploadAvatar);
};

export {profile};
