import {
  loginHandler,
  registerHandler,
  forgotPassHandler,
  resetPassHandler,
  changePassHandler,
} from './authService.js';

const loginForm = () => {
  document.querySelector('.container').innerHTML = '';

  const template = `
    <div class ="login-container">
      <h2>Login</h2>
      <div>
        <form action="#" id="login-form" class="login-form">
          <label for="email">Enter email</label>
          <input type="text" id="email" name="email" required >
          <label for="pass">Enter password</label>
          <input type="text" name="password" id="password" required>
          <button type="submit">Login</button>
        </form>
        <span>or</span>
        <a href="#" class="register-link">register</a>
        <a href="#" class="forget-pass-link">Forgot password?</a>
      </div>
    </div>
    `;

  document.querySelector('.container').innerHTML = template;

  document
      .getElementById('login-form')
      .addEventListener('submit', loginHandler);

  document
      .querySelector('.register-link')
      .addEventListener('click', registerForm);
  document
      .querySelector('.forget-pass-link')
      .addEventListener('click', forgotPassForm);
};

const registerForm = () => {
  document.querySelector('.container').innerHTML = '';

  const template = `
    <div class ="register-container">
      <h2>Register</h2>
      <div>
        <form action="#" id="register-form" class="register-form">
          <label for="email">Enter email</label>
          <input type="text" id="email" name="email" require >
          <label for="pass">Enter password</label>
          <input type="text" name="password" id="password" require>
          <div>
            <input type="radio" id="driver" name="role" value="DRIVER">
            <label for="driver">Driver</label>
            <input type="radio" id="shipper" name="role" value="SHIPPER">
            <label for="shipper">Shipper</label>
          </div>
          <button type="submit">Register</button>
        </form>
      </div>
    </div>
  `;

  document.querySelector('.container').innerHTML = template;

  document
      .getElementById('register-form')
      .addEventListener('submit', registerHandler);
};

const forgotPassForm = () => {
  document.querySelector('.container').innerHTML = '';

  const template = `
    <div class ="register-container">
      <h2>Forgot password</h2>
      <div>
        <h4>Reset link will be sent to your email</h4>
        <form action="#" id="forgot-pass-form" class="forgot-pass-form">
          <label for="email">Enter valid email</label>
          <input type="text" id="email" name="email" required >
          <button type="submit">Send</button>
        </form>
      </div>
    </div>
  `;
  document.querySelector('.container').innerHTML = template;

  document
      .getElementById('forgot-pass-form')
      .addEventListener('submit', forgotPassHandler);
};

const resetPassForm = () => {
  document
      .getElementById('reset-pass-form')
      .addEventListener('submit', resetPassHandler);
};

const changePassForm = () => {
  document.querySelector('.container').innerHTML = '';

  const template = `
    <div class ="register-container">
      <h2>Forgot password</h2>
      <div>
        <h4>Reset link will be sent to your email</h4>
        <form action="#" id="change-pass-form" class="forgot-pass-form">
          <label for="oldpass">Enter old password</label>
          <input type="text" id="oldpass" name="oldpass" required >
          <label for="newpass">Enter new password</label>
          <input type="text" id="newpass" name="newpass" required >
          <button type="submit">Send</button>
        </form>
      </div>
    </div>
  `;
  document.querySelector('.container').innerHTML = template;
  document
      .getElementById('change-pass-form')
      .addEventListener('submit', changePassHandler);
};

export {
  registerForm,
  loginForm,
  forgotPassForm,
  resetPassForm,
  changePassForm,
};
