import {loadsList} from './loads.js';
import {truckList} from './trucks.js';
import {pagination} from './pagination.js';
import {app} from './app.js';

const offset = 0;
const limit = 5;

/* loads*/

function createLoad(e) {
  e.preventDefault();

  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const body = new FormData(e.target);

  const raw = JSON.stringify({
    name: body.get('name'),
    payload: body.get('payload'),
    pickup_address: body.get('pickup'),
    delivery_address: body.get('deliver'),
    dimensions: {
      width: body.get('width'),
      length: body.get('length'),
      height: body.get('height'),
    },
  });

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/loads/', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert(result.message);
        app();
      })
      .catch((error) => console.log('error', error));
}
function postLoad(e) {
  const buttons = e.currentTarget.parentNode;
  const loadRow = buttons.parentNode;

  console.log(loadRow);
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    redirect: 'follow',
  };

  fetch(
      `http://localhost:8080/api/loads/${loadRow.dataset.id}/post`,
      requestOptions,
  )
      .then((response) => response.json())
      .then((result) => {
        alert(result.message);
        app();
      })
      .catch((error) => console.log('error', error));
}
function getAllLoads() {
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  const url = 'http://localhost:8080/api/loads/';

  fetch(`${url}?limit=${limit}&offset=${offset}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        loadsList(result);
        const nPerPage = result.loads.length;
        const pages = Math.ceil(result.total / nPerPage);
        pagination(url, pages, nPerPage);
      })
      .catch((error) => console.log('error', error));
}
function getNewLoads() {
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  const url = 'http://localhost:8080/api/loads?status=NEW';

  fetch(`${url}&limit=${limit}&offset=${offset}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        loadsList(result);
        const nPerPage = result.loads.length;
        const pages = Math.ceil(result.total / nPerPage);
        pagination(url, pages, nPerPage);
      })
      .catch((error) => console.log('error', error));
}
function getPostedLoads(e) {
  console.log(e);
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/loads?status=POSTED', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        loadsList(result);
      })
      .catch((error) => console.log('error', error));
}
function getAssignedLoads() {
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  const url = 'http://localhost:8080/api/loads?status=ASSIGNED';

  fetch(url, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        loadsList(result);

        const nPerPage = result.loads.length;
        const pages = Math.ceil(result.total / nPerPage);
        pagination(url, pages, nPerPage);
      })
      .catch((error) => console.log('error', error));
}
function getHistoryLoads() {
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/loads?status=SHIPPED', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        loadsList(result);
      })
      .catch((error) => console.log('error', error));
}
function chageLoadStatus() {
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);

  const requestOptions = {
    method: 'PATCH',
    headers: myHeaders,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/loads/active/state', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert(result.message);
        getMyLoads();
      })
      .catch((error) => console.log('error', error));
}

/* trucks*/

function createTruck(e) {
  e.preventDefault();

  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const body = new FormData(e.target);

  const raw = JSON.stringify({
    type: body.get('type'),
  });

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/trucks/', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert(result.message);
        app();
      })
      .catch((error) => console.log('error', error));
}
function getAllTrucks() {
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/trucks', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        truckList(result);
        console.log(result);
      })
      .catch((error) => console.log('error', error));
}
function assignTruck(e) {
  const buttons = e.currentTarget.parentNode;
  const truckRow = buttons.parentNode;

  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    redirect: 'follow',
  };

  fetch(
      `http://localhost:8080/api/trucks/${truckRow.dataset.id}/assign`,
      requestOptions,
  )
      .then((response) => response.json())
      .then((result) => {
        alert(result.message);
        app();
      })
      .catch((error) => console.log('error', error));
}
function getAssignedTrucks() {}
function getMyLoads() {
  getAllLoads();
}

//

function viewLoad() {}
function editLoad() {}

// calculations

function timerMinutes(date) {
  const today = new Date();
  const createDate = new Date(date);

  const seconds = Math.floor((today - createDate) / 1000);
  const interval = seconds / 60;

  const result = Math.floor(interval);

  if (result > 60) {
    return Math.floor(result / 60) + ' hours ago';
  }

  return Math.floor(interval) + ' minutes ago';
}

export {
  createLoad,
  postLoad,
  getAllLoads,
  getNewLoads,
  getPostedLoads,
  getAssignedLoads,
  getHistoryLoads,
  chageLoadStatus,
  viewLoad,
  editLoad,
  timerMinutes,
  createTruck,
  getAllTrucks,
  getAssignedTrucks,
  getMyLoads,
  assignTruck,
};
