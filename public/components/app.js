import {
  getAllLoads,
  getNewLoads,
  getPostedLoads,
  getAssignedLoads,
  getHistoryLoads,
  getAllTrucks,
  getAssignedTrucks,
  getMyLoads,
} from './appService.js';

import {logOut, goToProfile} from './userService.js';
import {loadCreateView} from './loads.js';
import {truckCreateView} from './trucks.js';

const header = (parent) => {
  const headerContainer = document.createElement('div');
  headerContainer.classList.add('header');

  const template = `
    <div class="logo">Some logo</div>
    <h3>Welcome ${window.localStorage.getItem('role')}</h3>
    <nav>
      <a href="#" class="header-logout-link">Log out</a>
      <a href="#" class="header-profile-link">Profile</a>
    </nav>
  `;
  headerContainer.innerHTML = template;
  document.querySelector('.app-container').append(headerContainer);

  document
      .querySelector('.header-logout-link')
      .addEventListener('click', logOut);
  document
      .querySelector('.header-profile-link')
      .addEventListener('click', goToProfile);
};

const nav = (parent, role) => {
  const navContainer = document.createElement('div');
  navContainer.classList.add('nav-menu');

  const currentRole = role;
  let template = '';

  if (currentRole === 'SHIPPER') {
    template = `
      <div class="operations">
        <button class="create-load">Create load</button>
      </div>
      <nav>
        <ul class="nav-ul">
          <li class="nav-li">
            <a href="#" class="get-all-loads">All loads</a>
          </li>
          <li class="nav-li">
            <a href="#" class="get-new-loads">New loads</a>
          </li>
          <li class="nav-li">
            <a href="#" class="get-posted-loads" >Posted loads</a>
          </li>
          <li class="nav-li">
            <a href="#" class="get-assigned-loads">Assigned loads</a>
          </li>
          <li class="nav-li">
            <a href="#" class="get-history-loads">History</a>
          </li>
        </ul>
      </nav>
    `;
  } else {
    template = `
    <div class="operations">
      <button class="create-truck">Create truck</button>
    </div>
      <nav>
        <ul class="nav-ul">
          <li class="nav-li">
            <a href="#" class="get-all-trucks">All trucks</a>
          </li>
          <li class="nav-li">
            <a href="#" class="get-assigned-trucks">Assigned trucks</a>
          </li>
          <li class="nav-li">
            <a href="#" class="get-my-loads">My loads</a>
          </li>
        </ul>
      </nav>
    `;
  }
  navContainer.innerHTML = template;

  document.querySelector('.app-container').append(navContainer);

  if (currentRole === 'SHIPPER') {
    document
        .querySelector('.create-load')
        .addEventListener('click', loadCreateView);
    document
        .querySelector('.get-all-loads')
        .addEventListener('click', getAllLoads);
    document
        .querySelector('.get-new-loads')
        .addEventListener('click', getNewLoads);
    document
        .querySelector('.get-posted-loads')
        .addEventListener('click', getPostedLoads);
    document
        .querySelector('.get-assigned-loads')
        .addEventListener('click', getAssignedLoads);
    document
        .querySelector('.get-history-loads')
        .addEventListener('click', getHistoryLoads);
  } else {
    document
        .querySelector('.create-truck')
        .addEventListener('click', truckCreateView);
    document
        .querySelector('.get-all-trucks')
        .addEventListener('click', getAllTrucks);
    document
        .querySelector('.get-assigned-trucks')
        .addEventListener('click', getAssignedTrucks);
    document
        .querySelector('.get-my-loads')
        .addEventListener('click', getMyLoads);
  }
};

const chart = (parent) => {
  const chartContainer = document.createElement('div');
  chartContainer.classList.add('chart-container');

  const template = `
    <div class="chart">
    
    </div>    
    <nav class="pagination-container"></nav>
  `;

  chartContainer.innerHTML = template;
  document.querySelector('.app-container').append(chartContainer);
};

function app() {
  const role = window.localStorage.getItem('role');

  const container = document.querySelector('.container');
  container.innerHTML = '';
  const appContainer = document.createElement('div');
  appContainer.classList.add('app-container');
  container.append(appContainer);

  header(appContainer);
  nav(appContainer, role);
  chart(appContainer);

  if (role === 'DRIVER') {
    getAllTrucks();
  } else {
    getAllLoads();
  }
}

export {app};
