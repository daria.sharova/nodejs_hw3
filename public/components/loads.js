import {
  viewLoad,
  editLoad,
  timerMinutes,
  createLoad,
  postLoad,
  chageLoadStatus,
} from './appService.js';

function loadsList(result) {
  const role = window.localStorage.getItem('role');

  const loadsContainer = document.querySelector('.chart');
  loadsContainer.classList.remove('trucks');
  loadsContainer.classList.add('loads');
  loadsContainer.innerHTML = '';

  if (!result.loads) {
    const noResult = document.createElement('h3');
    noResult.innerHTML = result.message;
    loadsContainer.append(noResult);
    return;
  }

  const chartHeader = document.createElement('div');
  chartHeader.innerHTML = '';
  chartHeader.classList.add('chart-header');

  loadsContainer.append(chartHeader);
  const headerTemplate = `
      <div>load name</div>
      <div>created date</div>
      <div>status</div>
      <div>pickup address</div>
      <div>deliver address</div>
    `;
  chartHeader.innerHTML = headerTemplate;

  result.loads.forEach((load) => {
    const loadRow = document.createElement('div');
    const loadName = document.createElement('div');
    const loadCreatedDate = document.createElement('div');
    const loadStatus = document.createElement('div');
    const loadPickUpA = document.createElement('div');
    const loadDeliveryA = document.createElement('div');
    const buttons = document.createElement('div');
    const viewButton = document.createElement('button');
    const editButton = document.createElement('button');
    const deleteButton = document.createElement('button');
    const shipInfoButton = document.createElement('button');
    const postLoadButton = document.createElement('button');
    const changeStatusButton = document.createElement('button');

    loadRow.classList.add('data-row');
    loadName.classList.add('load-name');
    loadCreatedDate.classList.add('load-created-date');
    loadStatus.classList.add('load-status');
    loadPickUpA.classList.add('load-pickup-a');
    loadDeliveryA.classList.add('load-delivery-a');
    buttons.classList.add('action-buttons');
    viewButton.classList.add('load-view');
    editButton.classList.add('load-edit');
    deleteButton.classList.add('load-delete');
    shipInfoButton.classList.add('load-shipment-info');
    postLoadButton.classList.add('load-post');
    changeStatusButton.classList.add('load-status');
    viewButton.title = 'view';
    editButton.title = 'edit';
    deleteButton.title = 'delete';
    shipInfoButton.title = 'shipment info';
    postLoadButton.title = 'post load';
    changeStatusButton.title = 'change load status';

    loadRow.dataset.id = load._id;

    if (role === 'SHIPPER') {
      changeStatusButton.style.display = 'none';
    } else {
      postLoadButton.style.display = 'none';
      shipInfoButton.style.display = 'none';
    }

    if (load.status !== 'NEW') {
      editButton.style.display = 'none';
      deleteButton.style.display = 'none';
      postLoadButton.style.display = 'none';
    } else {
      shipInfoButton.style.display = 'none';
    }

    buttons.append(
        changeStatusButton,
        postLoadButton,
        viewButton,
        editButton,
        deleteButton,
        shipInfoButton,
    );

    loadRow.append(
        loadName,
        loadCreatedDate,
        loadStatus,
        loadPickUpA,
        loadDeliveryA,
        buttons,
    );
    loadsContainer.append(loadRow);

    loadName.innerHTML = load.name;
    loadCreatedDate.innerHTML = timerMinutes(load.created_date);
    loadStatus.innerHTML = load.status;
    loadPickUpA.innerHTML = load.pickup_address;
    loadDeliveryA.innerHTML = load.delivery_address;
    changeStatusButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="M20,3V17.051c4.92-.557,5.453,6.758.5,6.949a3.516,3.516,0,0,1-2.853-5.514L.666,23.952l-.613-1.9,1.379-.443L.15,17.718a3,3,0,0,1,1.859-3.765l1.534,4.738L16,14.441v2.474l2-.644V3a3,3,0,0,1,3-3h3V2H21A1,1,0,0,0,20,3ZM4.817,16.13l10.7-3.625-1.8-5.444a3,3,0,0,0-3.806-1.9L5.008,6.84A3,3,0,0,0,3.1,10.623Z" fill="#ffffff" data-original="#000000"/></g></svg>`;
    postLoadButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="M9.5,7.5a2.5,2.5,0,0,1,5,0A2.5,2.5,0,0,1,9.5,7.5Zm4.12,15.565L17.328,20H18.5A5.507,5.507,0,0,0,24,14.5v-9A5.507,5.507,0,0,0,18.5,0H5.5A5.506,5.506,0,0,0,0,5.5v9A5.506,5.506,0,0,0,5.5,20H6.741l3.6,3.03a2.515,2.515,0,0,0,1.675.636A2.4,2.4,0,0,0,13.62,23.065ZM18.5,3A2.5,2.5,0,0,1,21,5.5v9A2.5,2.5,0,0,1,18.5,17H16.788a1.5,1.5,0,0,0-.956.344L12,20.511,8.255,17.353A1.5,1.5,0,0,0,7.289,17H5.5A2.5,2.5,0,0,1,3,14.5v-9A2.5,2.5,0,0,1,5.5,3ZM7.981,14.232A.665.665,0,0,0,8.654,15h6.661a.665.665,0,0,0,.673-.768C15.128,9.966,8.841,9.968,7.981,14.232Z" fill="#ffffff" data-original="#000000"/></g></svg>`;
    viewButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m16 6a1 1 0 0 1 0 2h-8a1 1 0 0 1 0-2zm7.707 17.707a1 1 0 0 1 -1.414 0l-2.407-2.407a4.457 4.457 0 0 1 -2.386.7 4.5 4.5 0 1 1 4.5-4.5 4.457 4.457 0 0 1 -.7 2.386l2.407 2.407a1 1 0 0 1 0 1.414zm-6.207-3.707a2.5 2.5 0 1 0 -2.5-2.5 2.5 2.5 0 0 0 2.5 2.5zm-4.5 2h-6a3 3 0 0 1 -3-3v-14a3 3 0 0 1 3-3h12a1 1 0 0 1 1 1v8a1 1 0 0 0 2 0v-8a3 3 0 0 0 -3-3h-12a5.006 5.006 0 0 0 -5 5v14a5.006 5.006 0 0 0 5 5h6a1 1 0 0 0 0-2z" fill="#ffffff" data-original="#000000"/></g></svg>`;
    editButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="M18.656.93,6.464,13.122A4.966,4.966,0,0,0,5,16.657V18a1,1,0,0,0,1,1H7.343a4.966,4.966,0,0,0,3.535-1.464L23.07,5.344a3.125,3.125,0,0,0,0-4.414A3.194,3.194,0,0,0,18.656.93Zm3,3L9.464,16.122A3.02,3.02,0,0,1,7.343,17H7v-.343a3.02,3.02,0,0,1,.878-2.121L20.07,2.344a1.148,1.148,0,0,1,1.586,0A1.123,1.123,0,0,1,21.656,3.93Z" fill="#ffffff" data-original="#000000"/><path xmlns="http://www.w3.org/2000/svg" d="M23,8.979a1,1,0,0,0-1,1V15H18a3,3,0,0,0-3,3v4H5a3,3,0,0,1-3-3V5A3,3,0,0,1,5,2h9.042a1,1,0,0,0,0-2H5A5.006,5.006,0,0,0,0,5V19a5.006,5.006,0,0,0,5,5H16.343a4.968,4.968,0,0,0,3.536-1.464l2.656-2.658A4.968,4.968,0,0,0,24,16.343V9.979A1,1,0,0,0,23,8.979ZM18.465,21.122a2.975,2.975,0,0,1-1.465.8V18a1,1,0,0,1,1-1h3.925a3.016,3.016,0,0,1-.8,1.464Z" fill="#ffffff" data-original="#000000"/></g></svg>`;
    deleteButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><g xmlns="http://www.w3.org/2000/svg" id="_01_align_center" data-name="01 align center"><path d="M22,4H17V2a2,2,0,0,0-2-2H9A2,2,0,0,0,7,2V4H2V6H4V21a3,3,0,0,0,3,3H17a3,3,0,0,0,3-3V6h2ZM9,2h6V4H9Zm9,19a1,1,0,0,1-1,1H7a1,1,0,0,1-1-1V6H18Z" fill="#ffffff" data-original="#000000"/><rect x="9" y="10" width="2" height="8" fill="#ffffff" data-original="#000000"/><rect x="13" y="10" width="2" height="8" fill="#ffffff" data-original="#000000"/></g></g></svg>`;
    shipInfoButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="M19,5H16.9A5.009,5.009,0,0,0,12,1H5A5.006,5.006,0,0,0,0,6v9a4,4,0,0,0,3.061,3.877,3.5,3.5,0,1,0,6.9.123h4.082a3.465,3.465,0,0,0-.041.5,3.5,3.5,0,0,0,7,0,3.4,3.4,0,0,0-.061-.623A4,4,0,0,0,24,15V10A5.006,5.006,0,0,0,19,5Zm3,5v1H17V7h2A3,3,0,0,1,22,10ZM2,15V6A3,3,0,0,1,5,3h7a3,3,0,0,1,3,3V17H4A2,2,0,0,1,2,15Zm6,4.5a1.5,1.5,0,0,1-3,0,1.418,1.418,0,0,1,.093-.5H7.907A1.418,1.418,0,0,1,8,19.5ZM17.5,21A1.5,1.5,0,0,1,16,19.5a1.41,1.41,0,0,1,.093-.5h2.814a1.41,1.41,0,0,1,.093.5A1.5,1.5,0,0,1,17.5,21ZM20,17H17V13h5v2A2,2,0,0,1,20,17Z" fill="#ffffff" data-original="#000000"/></g></svg>`;

    viewButton.addEventListener('click', viewLoad);
    editButton.addEventListener('click', editLoad);
    postLoadButton.addEventListener('click', postLoad);
    changeStatusButton.addEventListener('click', chageLoadStatus);
    // deleteButton.addEventListener('click', deleteLoad);
  });
}
function loadCreateView() {
  const loadsContainer = document.querySelector('.chart-container');
  loadsContainer.innerHTML = '';

  const template = `
  <h2>Create new load</h2>
   <form action="#" id="create-load-form" class="create-load-form">
    <label for="name">Load name</label>
    <input type="text" id="name" name="name" required>
    <label for="payload">Load payload</label>
    <input type="text" id="payload" name="payload" required>
    <label for="pickup">Load pick-up adress</label>
    <input type="text" id="pickup" name="pickup" required>
    <label for="deliver">Load delivery adress</label>
    <input type="text" id="deliver" name="deliver" required>
    <div class="create-load-dimentions">
      Load dimentions:
      <label for="width">width</label>
      <input type="text" id="width" name="width" required>
      <label for="length">length</label>
      <input type="text" id="length" name="length" required>
      <label for="height">height</label>
      <input type="text" id="height" name="height" required>
    </div>
    <button type="submit" class="create-load-button">Create Load</button>
   </form>
  `;
  loadsContainer.innerHTML = template;

  document
      .getElementById('create-load-form')
      .addEventListener('submit', createLoad);
}
function loadEditView(result) {
  const loadsContainer = document.querySelector('.chart');
  loadsContainer.innerHTML = '';

  console.log(result);
}

export {loadsList, loadEditView, loadCreateView};
