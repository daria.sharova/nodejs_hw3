import {loadsList} from './loads.js';

let urlGlobal;
let nPerPageGlobal;

function pagination(url, pages, nPerPage) {
  const paginationContainer = document.querySelector('.pagination-container');
  paginationContainer.innerHTML = '';
  const pageButtons = document.createElement('div');
  pageButtons.classList.add('page-numbers');
  pageButtons.innerHTML = '';

  for (let i = 1; i <= pages; i++) {
    const page = document.createElement('button');
    page.classList.add('page', `page-${i}`);
    page.innerHTML = i;
    pageButtons.append(page);
  }
  paginationContainer.append(pageButtons);

  urlGlobal = url;
  nPerPageGlobal = nPerPage;

  const firstPage = document.querySelector('.page-1');
  firstPage.classList.add('current');

  paginationContainer.addEventListener('click', paginatePage );
}

function paginatePage(e, url, nPerPage) {
  e.stopPropagation();

  const page = document.querySelectorAll('.page');

  page.forEach((e) => e.classList.remove('current'));

  if (e.target.classList[0] === 'page') {
    e.target.classList.add('current');
  }

  const limit = nPerPageGlobal;
  const offset = (e.target.innerHTML - 1) * limit;

  console.log(offset);
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  const firstConnector = urlGlobal.includes('?') ? '&' : '?';

  fetch(`${urlGlobal}${firstConnector}limit=${limit}&offset=${offset}`,
      requestOptions)
      .then((response) => response.json())
      .then((result) => {
        loadsList(result, url);
      })
      .catch((error) => console.log('error', error));
}

export {
  pagination,
};
