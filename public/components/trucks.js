import {createTruck, assignTruck} from './appService.js';

function truckList(result) {
  const trucksContainer = document.querySelector('.chart');
  trucksContainer.classList.remove('trucks');
  trucksContainer.classList.add('trucks');
  trucksContainer.innerHTML = '';

  if (!result.trucks) {
    const noResult = document.createElement('h3');
    noResult.innerHTML = result.message;
    trucksContainer.append(noResult);
    return;
  }

  const chartHeader = document.createElement('div');
  chartHeader.innerHTML = '';
  chartHeader.classList.add('chart-header');

  trucksContainer.append(chartHeader);
  const headerTemplate = `
      <div>truck type</div>
      <div>status</div>
      <div>truck payload</div>
      <div>assigned</div>
    `;
  chartHeader.innerHTML = headerTemplate;

  result.trucks.forEach((truck) => {
    const truckRow = document.createElement('div');
    const truckType = document.createElement('div');
    const truckStatus = document.createElement('div');
    const truckPaytruck = document.createElement('div');
    const truckIsAssigned = document.createElement('div');
    const buttons = document.createElement('div');
    const viewButton = document.createElement('button');
    const editButton = document.createElement('button');
    const assignButton = document.createElement('button');

    truckRow.classList.add('data-row');
    truckType.classList.add('truck-type');
    truckStatus.classList.add('truck-status');
    truckPaytruck.classList.add('truck-payload');
    truckIsAssigned.classList.add('truck-assigned');
    buttons.classList.add('action-buttons');
    viewButton.classList.add('truck-view');
    editButton.classList.add('truck-edit');
    assignButton.classList.add('assign-edit');
    viewButton.title = 'view';
    editButton.title = 'edit';
    assignButton.title = 'assign';

    truckRow.dataset.id = truck._id;

    buttons.append(viewButton, editButton, assignButton);

    truckRow.append(
        truckType,
        truckStatus,
        truckPaytruck,
        truckIsAssigned,
        buttons,
    );
    trucksContainer.append(truckRow);

    truckType.innerHTML = truck.type;
    truckStatus.innerHTML = truck.status;
    truckPaytruck.innerHTML = truck.payload;
    truckIsAssigned.innerHTML = truck.assigned_to ? 'yes' : 'no';
    viewButton.innerHTML =
      '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m16 6a1 1 0 0 1 0 2h-8a1 1 0 0 1 0-2zm7.707 17.707a1 1 0 0 1 -1.414 0l-2.407-2.407a4.457 4.457 0 0 1 -2.386.7 4.5 4.5 0 1 1 4.5-4.5 4.457 4.457 0 0 1 -.7 2.386l2.407 2.407a1 1 0 0 1 0 1.414zm-6.207-3.707a2.5 2.5 0 1 0 -2.5-2.5 2.5 2.5 0 0 0 2.5 2.5zm-4.5 2h-6a3 3 0 0 1 -3-3v-14a3 3 0 0 1 3-3h12a1 1 0 0 1 1 1v8a1 1 0 0 0 2 0v-8a3 3 0 0 0 -3-3h-12a5.006 5.006 0 0 0 -5 5v14a5.006 5.006 0 0 0 5 5h6a1 1 0 0 0 0-2z" fill="#ffffff" data-original="#000000"/></g></svg>';
    editButton.innerHTML =
      '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="M18.656.93,6.464,13.122A4.966,4.966,0,0,0,5,16.657V18a1,1,0,0,0,1,1H7.343a4.966,4.966,0,0,0,3.535-1.464L23.07,5.344a3.125,3.125,0,0,0,0-4.414A3.194,3.194,0,0,0,18.656.93Zm3,3L9.464,16.122A3.02,3.02,0,0,1,7.343,17H7v-.343a3.02,3.02,0,0,1,.878-2.121L20.07,2.344a1.148,1.148,0,0,1,1.586,0A1.123,1.123,0,0,1,21.656,3.93Z" fill="#ffffff" data-original="#000000"/><path xmlns="http://www.w3.org/2000/svg" d="M23,8.979a1,1,0,0,0-1,1V15H18a3,3,0,0,0-3,3v4H5a3,3,0,0,1-3-3V5A3,3,0,0,1,5,2h9.042a1,1,0,0,0,0-2H5A5.006,5.006,0,0,0,0,5V19a5.006,5.006,0,0,0,5,5H16.343a4.968,4.968,0,0,0,3.536-1.464l2.656-2.658A4.968,4.968,0,0,0,24,16.343V9.979A1,1,0,0,0,23,8.979ZM18.465,21.122a2.975,2.975,0,0,1-1.465.8V18a1,1,0,0,1,1-1h3.925a3.016,3.016,0,0,1-.8,1.464Z" fill="#ffffff" data-original="#000000"/></g></svg>';
    assignButton.innerHTML =
      '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="M9.5,7.5a2.5,2.5,0,0,1,5,0A2.5,2.5,0,0,1,9.5,7.5Zm4.12,15.565L17.328,20H18.5A5.507,5.507,0,0,0,24,14.5v-9A5.507,5.507,0,0,0,18.5,0H5.5A5.506,5.506,0,0,0,0,5.5v9A5.506,5.506,0,0,0,5.5,20H6.741l3.6,3.03a2.515,2.515,0,0,0,1.675.636A2.4,2.4,0,0,0,13.62,23.065ZM18.5,3A2.5,2.5,0,0,1,21,5.5v9A2.5,2.5,0,0,1,18.5,17H16.788a1.5,1.5,0,0,0-.956.344L12,20.511,8.255,17.353A1.5,1.5,0,0,0,7.289,17H5.5A2.5,2.5,0,0,1,3,14.5v-9A2.5,2.5,0,0,1,5.5,3ZM7.981,14.232A.665.665,0,0,0,8.654,15h6.661a.665.665,0,0,0,.673-.768C15.128,9.966,8.841,9.968,7.981,14.232Z" fill="#ffffff" data-original="#000000"/></g></svg>';

    // viewButton.addEventListener('click', viewTruck);
    // editButton.addEventListener('click', editTruck);
    assignButton.addEventListener('click', assignTruck);
  });
}

function truckCreateView() {
  const trucksContainer = document.querySelector('.chart-container');
  trucksContainer.innerHTML = '';

  const template = `
  <h2>Create new truck</h2>
   <form action="#" id="create-truck-form" class="create-truck-form">
   Please choose your vehicle type:
    <div>
      <label for="sprinter">Sprinter</label>
      <input type="radio" id="sprinter" name="type" value="SPRINTER">
      <label for="small-straight">Small Straight</label>
      <input type="radio" id="small-straight" name="type value="SMALL STRAIGHT">
      <label for="large-straight">Large Straight</label>
      <input type="radio" id="large-straight" name="type" value="LARGE STRAIGHT">
    </div>
    <button type="submit" class="create-truck-button">Create truck
    </button>
   </form>
  `;
  trucksContainer.innerHTML = template;

  document
      .getElementById('create-truck-form')
      .addEventListener('submit', createTruck);
}

export {truckList, truckCreateView};
