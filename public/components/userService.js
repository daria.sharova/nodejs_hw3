import {profile} from './user.js';

function logOut() {
  localStorage.removeItem('jwt_token');
  document.location.reload();
}

function goToProfile() {
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/users/me', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        profile(result.user.role, result.user.email, 'uploads/' + result.user.image);
      })
      .catch((error) => console.log('error', error));
}

function deleteAccount() {
  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);

  const requestOptions = {
    method: 'DELETE',
    headers: myHeaders,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/users/me', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert(result.message);
        logOut();
      })
      .catch((error) => console.log('error', error));
}

function uploadAvatar(e) {
  e.preventDefault();

  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);

  const formdata = new FormData(e.target);

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: formdata,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/users/me', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        document.querySelector('.change-avatar').style.display = 'none';
        goToProfile();
      })
      .catch((error) => console.log('error', error));
}


export {logOut, goToProfile, deleteAccount, uploadAvatar};
