import {loginForm} from './auth.js';
import {app} from './app.js';

function loginHandler(e) {
  e.preventDefault();

  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const body = new FormData(e.target);

  const raw = JSON.stringify({
    email: body.get('email'),
    password: body.get('password'),
  });

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch('/api/auth/login', requestOptions)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        return response.json().then((body) => {
          console.log(body);
          throw new Error(body.message);
        });
      })
      .then((result) => {
        console.log('result', result);
        alert(result.message);
        if (result) {
          window.localStorage.setItem('jwt_token', result.jwt_token);
          window.localStorage.setItem('role', result.role);
          app();
        }
      })
      .catch((error) => {
        alert(error.message);
        loginForm();
      });
}

function registerHandler(e) {
  e.preventDefault();

  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const body = new FormData(e.target);

  const raw = JSON.stringify({
    email: body.get('email'),
    password: body.get('password'),
    role: body.get('role'),
  });

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/auth/register', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result.message);
        alert(result.message);
        loginForm();
      })
      .catch((error) => console.log('error', error));
}

function forgotPassHandler(e) {
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const body = new FormData(e.target);

  const raw = JSON.stringify({
    email: body.get('email'),
  });

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/auth/forgot_password', requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log('error', error));
}

function resetPassHandler(e) {
  e.preventDefault();

  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const body = new FormData(e.target);
  console.log(body);

  const raw = JSON.stringify({
    password: body.get('password'),
  });

  console.log(raw);

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  const link = window.location.href;

  fetch(link, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        alert(result.message);
        window.localStorage.removeItem('jwt_token');
        loginForm();
      })
      .catch((error) => console.log('error', error));
}

function changePassHandler(e) {
  e.preventDefault();

  const jwt_token = window.localStorage.getItem('jwt_token');

  const myHeaders = new Headers();
  myHeaders.append('Authorization', 'Bearer ' + jwt_token);
  myHeaders.append('Content-Type', 'application/json');

  const body = new FormData(e.target);

  const raw = JSON.stringify({
    oldPassword: body.get('oldpass'),
    newPassword: body.get('newpass'),
  });

  const requestOptions = {
    method: 'PATCH',
    headers: myHeaders,
    body: raw,
    redirect: 'follow',
  };

  fetch('http://localhost:8080/api/users/me/password', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        app();
      })
      .catch((error) => console.log('error', error));
}

export {
  loginHandler,
  registerHandler,
  forgotPassHandler,
  resetPassHandler,
  changePassHandler,
};
