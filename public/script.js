import {loginForm} from './components/auth.js';
import {app} from './components/app.js';

// controller

const token = window.localStorage.getItem('jwt_token');

if (token) {
  app();
} else {
  loginForm();
}
